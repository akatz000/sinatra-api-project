import React from 'react';
import ReactDOM from 'react-dom';
import AppMonitor from './AppMonitor';

ReactDOM.render(
  <React.StrictMode>
    <AppMonitor url='http://localhost:4567/v1/all-status'/>
  </React.StrictMode>,
  document.getElementById('root')
);
