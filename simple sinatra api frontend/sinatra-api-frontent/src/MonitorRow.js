function MonitorRow(props) {
  return (
    <tr>
      <td>{props.data.url}</td>
      <td>{props.data.status}</td>
      <td>{props.data.duration}</td>
      <td>{props.data.timestamp}</td>
    </tr>
  )
}

export default MonitorRow;
