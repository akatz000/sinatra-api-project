import { useState, useEffect, useRef } from 'react';
import useInterval from './useInterval';
import MonitorRow from './MonitorRow';
import axios from 'axios';
import './AppMonitor.css'

function AppMonitor(props) {
  const [statuses, setStatuses] = useState([]);

  useEffect(() => {
    const getData = async () => {
      let data = await axios.get(props.url);
      setStatuses(data.data);
    }

    getData();
  }, []);

  useInterval(async() => {
    let response = await axios.get(props.url);
    setStatuses(response.data);
  }, 60000);

  return (
    <table>
      <thead>
        <tr>
          <th>URL</th>
          <th>Status</th>
          <th>Duration</th>
          <th>Timestamp</th>
        </tr>
      </thead>
      <tbody>
        { statuses.map((item, i) => {
          return (<MonitorRow key={i} data={item}/>)
        })}
      </tbody>
    </table>
  );
}

export default AppMonitor;
