# Sinatra API Project

### Overview

The backend is a simple Sinatra app.  The URL of the site to check or the desire to check all
'registered' sites is extracted and transformed from the path.  The StatusChecker class handles
all of the lifting without state or side effects, and exposes a minimalist interface.

The front end is a ReactJS app using functional components.  Hooks are used to update the UI,
and make the repeating requests to the API back end.

### Instructions

The folder under 'simple sinatra api frontend' contains a ReactJS app that is a
super simple UI for this project.

The 'simple sinatra api' folder contains a sinatra ruby app that is the API backend
for the project.

Spin up the backend with `ruby .\app.rb` from the backend project folder.

Launch the react frontend with `yarn start` from the UI project folder.

localhost:3000 should be brought up by running `yarn start` but if not, visit
localhost:3000 to see the project working.
