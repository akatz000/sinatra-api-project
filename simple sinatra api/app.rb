require 'sinatra'
require 'json'
require 'http'
require_relative './status_checker'

before do
  response.headers["Access-Control-Allow-Origin"] = "*"
end

options "*" do
  response.headers["Allow"] = "HEAD,GET,OPTIONS"
  200
end

get '/v1/:status' do
  content_type :json
  urls = {
    'amazon-status' => 'https://www.amazon.com',
    'google-status' => 'https://www.google.com',
  }
  if params[:status] == 'all-status'
    resp = []
    urls.each_value do |url_to_check|
      resp << StatusChecker.new.call(url_to_check)
    end
  elsif urls[params[:status]]
    resp = StatusChecker.new.call(url_to_check)
  else
    return 400
  end

  resp.to_json
end
