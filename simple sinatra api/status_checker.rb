require 'http'

class StatusChecker
  def call url_to_check
    data = make_request url_to_check
    build_response data
  end

  private

  def make_request url_to_check
    start_time = Time.now
    response = HTTP.get(url_to_check)
    duration = ((Time.now - start_time) * 1000).round
    {
      response: response,
      duration: duration,
    }
  end

  def build_response data
    {
      url: data[:response].uri,
      status: data[:response].status,
      duration: data[:duration],
      timestamp: Time.now.to_i,
    }
  end
end
